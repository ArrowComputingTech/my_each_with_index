#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def my_each_with_index
    len = self.length
    i = 0
    len.times do |x|
      yield(self[x], i)
      i += 1
    end
    return self
  end
end

['2','3','6','4'].my_each_with_index do |x,index|
  puts "x: #{x}"
  puts "x Doubled: " + (x.to_i * 2).to_s
  puts "x is at position: #{index}"
end
